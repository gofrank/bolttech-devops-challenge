## Description

This is a backend service that should never be publicly exposed. Used by the API service.

### Endpoints

Exposes the following endpoints:

- `/` - Will return a json response with data;
- `/healthcheck` - Will return 200 if the service is healthy, 500 otherwise.

### Configuration
The following configurations should be provided to the service as environment variables:

- `DATABASE_CONNECTION_STRING` - Connection string to the service's MongoDB Database;
- `ENVIRONMENT` - Should be 'development' or 'production';
- `PORT`- Listen port;


### Logs
All logs from this service are sent to stdout and stderr.

### Database
This service requires a MongoDB Database (or compatible). 
The connection string to the database should be provided using the respective configuration option above.

