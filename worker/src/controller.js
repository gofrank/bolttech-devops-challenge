import HealthcheckModel from "./healthcheck.model.js";
const ENVIRONMENT = process.env.ENVIRONMENT || "development";

const getData = async (_req, res) => {
  let data = {};

  try {
    const count = await HealthcheckModel.countDocuments();
    const last = await HealthcheckModel.findOne({}, [], {
      sort: { checkDate: -1 },
    });
    data = {
      count,
      lastCheck: last.checkDate,
    };
  } catch (error) {
    console.error("Could not get data from database.", error);
  }

  res.json({
    message: "Hello from worker!",
    data,
    environment: ENVIRONMENT,
  });
};

const healthcheck = async (_req, res) => {
  try {
    const check = new HealthcheckModel();
    await check.save();
    let message = "Worker is healthy.";

    return res.status(200).json({
      healthy: true,
      status: message,
    });
  } catch (error) {
    let message = `Service not healthy: ${error.message}`;
    console.error(`Healthcheck - ${message}`, error)
    return res.status(500).json({
      healthy: false,
      status: message,
    });
  }
};

export default {
  getData,
  healthcheck,
};
