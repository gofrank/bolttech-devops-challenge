import express from "express";
import cors from "cors";
import database from "./database.js";
import controller from "./controller.js";

const PORT = process.env.PORT;
const app = express();
app.use(cors());
database.connect();

app.get("/", controller.getData);
app.get("/healthcheck", controller.healthcheck);

app.get("*", function (req, res) {
  res.sendStatus(404);
});

app.listen(PORT, () => {
  console.log("Starting node.js on port " + PORT);
});
