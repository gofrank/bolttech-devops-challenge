import mongoose from "mongoose";

const DATABASE_CONNECTION_STRING = process.env.DATABASE_CONNECTION_STRING;

const connect = () => {
  if (!DATABASE_CONNECTION_STRING) {
    console.error("Database connection string is not defined.");
    process.exit();
  }

  mongoose.connect(DATABASE_CONNECTION_STRING, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
  const db = mongoose.connection;
  db.on("error", console.error.bind(console, "connection error:"));
  db.once("open", function () {
    console.log("Connected to MongoDB!");
  });
};

export default { connect };
