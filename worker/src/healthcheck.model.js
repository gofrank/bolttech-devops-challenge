import mongoose from "mongoose";

const HealthcheckModel = mongoose.model(
  "Healthcheck",
  new mongoose.Schema({
    checkDate: { type: Date, required: true, default: Date.now },
  })
);

export default HealthcheckModel;
