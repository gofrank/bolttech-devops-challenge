import fetch from "node-fetch";

const ENVIRONMENT = process.env.ENVIRONMENT || "development";
const WORKER_URL = process.env.WORKER_URL;
const WORKER_PROTOCOL = process.env.WORKER_PROTOCOL;
const WORKER_PORT = process.env.WORKER_PORT;

const getData = async (_req, res) => {
  let workerData = {};
  let code = 200;

  try {
    const result = await fetch(
      `${WORKER_PROTOCOL}://${WORKER_URL}:${WORKER_PORT}`
    );
    workerData = await result.json();
  } catch (error) {
    console.error("Could not get data from worker.", error);
    workerData = {
      message: "Could not get data from worker",
      error: error.message
    };
    code = 500;
  }

  res.status(code).json({
    message: code == 200? "Hello, world!": "Ooops.. Something went wrong!",
    workerData,
    environment: ENVIRONMENT,
  });
};

const healthcheck = async (_req, res) => {
  try {
    if (!WORKER_URL || !WORKER_PORT || !WORKER_PROTOCOL) {
      throw new Error("Environment variables are not properly set.");
    }

    let workerResult = {};
    const workerHealthcheckUrl = `${WORKER_PROTOCOL}://${WORKER_URL}:${WORKER_PORT}/healthcheck`;

    try {
      const result = await fetch(
        workerHealthcheckUrl,
        { signal: AbortSignal.timeout( 1000 ) }
      );
      workerResult = await result.json();

    } catch (error) {
      console.error(`Error connecting to worker at ${workerHealthcheckUrl}`, error);
    }

    const status = workerResult.healthy
      ? "API is working as expected."
      : "API is up but may have unexpected results because API cannot connect to worker.";

    let result = {
      healthy: true,
      workerHealthy: workerResult.healthy || false,
      status
    };

    return res.status(200).json(result);
  } catch (error) {
    const message = `Service not ready: ${error.message}`;
    console.error("Healthcheck - ", message);

    let result = {
      healthy: false,
      status: message,
    };
    return res.status(500).json(result);
  }
};

export default {
  getData,
  healthcheck,
};
