import express from "express";
import cors from "cors";
import controller from "./controller.js"
import { execSync } from 'child_process';

console.log("Starting API, please wait...")

execSync('sleep 30');

const PORT = process.env.PORT;

const app = express();
app.use(cors());

app.get("/api", controller.getData);
app.get("/healthcheck", controller.healthcheck);

app.get("*", function (req, res) {
  res.sendStatus(404);
});

app.listen(PORT, () => {
  console.log("Starting node.js on port " + PORT);
});
