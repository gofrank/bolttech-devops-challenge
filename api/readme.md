## Description
Main API for our application, implemented in NodeJS.

### Enpoints
Exposes the following endpoints:

- `/api` - Will return a json response with data;
- `/healthcheck` - Will return 200 if the service is healthy, 500 otherwise.


### Configuration
The following configurations should be provided to the service as environment variables:

- `WORKER_URL` - The URL for the worker service;
- `WORKER_PROTOCOL` - The protocol to use to connect to the worker service (http or https);
- `WORKER_PORT` - The port to use to connect to the worker service;
- `ENVIRONMENT` - Should be 'development' or 'production';
- `PORT`- Listen port;

### Logs
All logs from this service are sent to stdout and stderr.
